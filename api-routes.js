// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!'
    });
});

var userController = require('./controllers/userController');

//User routes
router.route('/users')
    .get(userController.get_users)
    .post(userController.new_user);


router.route('/users/:card_number')
    .get(userController.get_user_credits)
    .post(userController.update);



// Export API routes
module.exports = router;