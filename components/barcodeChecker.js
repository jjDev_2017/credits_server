SodaCan = require('../models/sodaCanModel');

/*
    Will attempt to save a record of a can with barcode
    If barcode is not unique, mongoose will throw an error
*/
const barcodeIsUnique = async(barcode) => {    
    var result = false;
    var sodaCan = new SodaCan();
    sodaCan.barcode = barcode;

    try{
        await sodaCan.save();
        result = true;
    }catch(err){
        console.log(err);
    }
    console.log("barcodeIsUnique is returning: " + result);
    return result;
}

module.exports = {barcodeIsUnique};