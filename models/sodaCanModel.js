var mongoose = require('mongoose');
var sodaCanSchema = mongoose.Schema({
    barcode: {
        type: String,
        required: true,
        unique: true
    }
});

// Export SodaCan model
var SodaCan = module.exports = mongoose.model('sodaCan', sodaCanSchema);
module.exports.get = function (callback, limit) {
    SodaCan.find(callback).limit(limit);
}