User = require('../models/userModel');
SodaCan = require('../models/sodaCanModel');
const barcodeChecker =  require('../components/barcodeChecker');


exports.get_users = function (req, res) {
    User.get(function (err, users) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Users retrieved successfully",
            users_list: users
        });
    });
};


exports.new_user = function(req, res){     
    var user = new User();
    user.card_number = req.body.card_number;
    user.credits = 0;

    // save the user and check for errors
    user.save(function (err) {
        if (err){
            console.log(err.message);
            return res.json({status: 500, error: err});
        }
        res.json({
            message: 'New user created!',
            data: user
        });
    });
}

//get a user's current credit amount
exports.get_user_credits = function (req, res) {
    console.log("Inside userController.get_user_credits");
    User.find({card_number: req.params.card_number}, function (err, user) {
        if (err)
            res.send(err);
        res.json({
            message: 'User details loading..',
            user: user
        });
    });
};

// Handle update contact info
// /users/:user_id
exports.update = async (req, res) => {
    console.log("in userController.update");
    User.findOne({card_number: req.params.card_number}, async function (err, userFoundObj) {
        console.log("user found: " + userFoundObj);
        if (err){
            return res.json({status: 500, error: err});
        }                    

        // If NOT unique barcode then send an error message
        var barcodeIsNew = await barcodeChecker.barcodeIsUnique(req.body.barcode);
        console.log("==> " + barcodeIsNew);
        if(!barcodeIsNew){
            return res.status(201).send({message:'Soda Can cannot be redeemed twice'});
        }

        if(userFoundObj){
            //if undefined credits
            if(!userFoundObj.credits){
                userFoundObj.credits = 0;
            }        
            userFoundObj.credits += 5;
                       
        }else{
            res.status(500).send("No User found with card_number: " + req.params.card_number);
        }

        userFoundObj.save(function(err, updatedObject){
            if(err){
                console.log(err);
                res.status(500).send(err);
            }else{
                res.json({
                    message: 'Thanks for Recycling! You now have: ' + updatedObject.credits + ' credits.',
                    user: updatedObject
                }); 
            }
        });

    });    
};